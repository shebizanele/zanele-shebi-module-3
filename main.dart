import 'dart:html';

import 'package:flutter/material.dart';
import 'profile.dart';
import 'loginscreen.dart';
import 'dash.dart';

void main() {
  runApp(const FirstApp());
}

class FirstApp extends StatelessWidget {
  const FirstApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Module Three',
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
      theme: ThemeData(
          primarySwatch: Colors.indigo,
          accentColor: Colors.indigo,
          scaffoldBackgroundColor: Colors.white),
    );
  }
}
